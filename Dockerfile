FROM python:3.7

ENV API_KEY=

ENV API_SECRET_KEY=

ENV ACCESS_TOKEN_KEY=

ENV ACCESS_TOKEN_SECRET=

ADD requirements-for-docker.txt ./
ADD scraper ./

RUN pip install -r requirements-for-docker.txt

CMD ["python", "-u", "main.py"]

