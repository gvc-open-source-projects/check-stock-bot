import abc


class Bot(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def post_text(self, text: str):
        raise NotImplementedError("Function not yet implemented")