import os
from pathlib import Path

import twitter
from dotenv import load_dotenv, find_dotenv

from scraper.bots.bot_interface import Bot


class TwitterBot(Bot):
    api = None
    access_granted = False

    def __init__(self):
        try:
            load_dotenv(find_dotenv())
        except IOError:
            print(".env file was not found.")

        consumer_key = os.getenv("API_KEY")
        consumer_secret = os.getenv("API_SECRET_KEY")
        access_token_key = os.getenv("ACCESS_TOKEN_KEY")
        access_token_secret = os.getenv("ACCESS_TOKEN_SECRET")

        self.api = twitter.Api(consumer_key=consumer_key, consumer_secret=consumer_secret,
                               access_token_key=access_token_key, access_token_secret=access_token_secret)
        self.api.VerifyCredentials()

    def post_text(self, text: str):
        try:
            print(f"will post following test: {text}")
            self.api.PostUpdate(text)
        except twitter.error.TwitterError as e:
            print("there was a twitter error.")
            print(e.message)