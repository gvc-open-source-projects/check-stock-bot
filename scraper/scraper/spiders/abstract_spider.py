import datetime
import os
from abc import abstractmethod, ABCMeta
from enum import Enum

from dotenv import load_dotenv, find_dotenv

from scraper.bots.bot_interface import Bot
from scraper.bots.twitter import TwitterBot


class GraphicsCardEnum(Enum):
    RTX_3070 = "#RTX3070"
    RTX_3080 = "#RTX3080"
    RTX_3090 = "#RTX3090"


class AbstractSpider(metaclass=ABCMeta):
    """This class contains helper methods."""

    webshop_name = "Not filled in"
    post_if_no_stock = False
    twitter_bot: Bot = None
    debug: bool = False

    def __init__(self, init_twitter: bool = True):
        """Will default init twitter and set post_if_no_stock to False."""
        try:
            load_dotenv(find_dotenv())
        except IOError:
            print(".env file was not found.")

        self.post_if_no_stock = os.getenv("POST_IF_NO_STOCK", "False") == "True"
        self.debug = os.getenv("DEBUG", "False") == "True"
        if init_twitter:
            self.twitter_bot = TwitterBot()

    def set_webshop_name(self, name) -> None:
        """Will set the webshop name and use it in the posts about it.
        :rtype: None
        :param name: name of webshop
        """
        self.webshop_name = name

    def get_webshop_name(self) -> str:
        """Gets the name of the webshop."""
        return self.webshop_name

    def get_emojis_for_status(self, status: bool) -> str:
        """Gets emojis based on the status.
        :param status: if true then gud status and if false then not gud status.
        :return:
        """
        if status:
            return "✔✔✔"
        return "❌❌❌"

    def datetime_to_string(self, datetimestamp: datetime.datetime):
        return f"{datetimestamp.strftime('%d/%m/%Y, %H:%M:%S')}"

    @abstractmethod
    def get_tags_to_add_to_tweet(self, model: GraphicsCardEnum):
        raise NotImplementedError()

    def get_common_tags(self):
        return ""

    def create_tweet(
        self,
        description: str,
        emojis: str,
        utc_time: str,
        graphics_card_model: str,
        store: str,
        tags: str,
        url: str,
        price: float = None,
    ):
        """Will make a generic tweet string and return it."""
        return (
            f"Status: {description} {emojis} \n"
            f"Model: {graphics_card_model} \n"
            f"Store: #{store}\n"
            f"Price: { round(price, 2) if price is not None else 'unknown'}"
            f"Tags: {tags} {self.debug}\n"
            f"URL: {url}"
        )

    def report(
        self,
        is_there_stock: bool,
        description: str,
        utc_time: datetime.datetime,
        url: str,
        model: GraphicsCardEnum,
        webshop_name: str,
    ):
        """Generic function that will report the stock and
        send it to all the possible bots. Everything will also be included.

        !!! ONLY TWITTER ATM !!!
        :param model: the graphics card model
        :param is_there_stock: boolean to indicate if there is stock
        :param description: description for message.
        :param utc_time: time the stock was determined.
        :param url: the url the stock was determined on.
        :param webshop_name: the name of the webshop.
        :return:
        """
        print("will report")
        if self.post_if_no_stock is False and is_there_stock is False:
            print("nothing reported because there is no stock.")
            return
        if self.twitter_bot is not None:
            print(f"reporting to twitter for {webshop_name}")
            self.twitter_bot.post_text(
                self.create_tweet(
                    description,
                    self.get_emojis_for_status(is_there_stock),
                    self.datetime_to_string(utc_time),
                    model.value,
                    webshop_name,
                    self.get_tags_to_add_to_tweet(model),
                    url,
                )
            )
