import datetime

import scrapy
from scrapy.http import HtmlResponse
from scraper.spiders.abstract_spider import AbstractSpider, GraphicsCardEnum


class AlternateNlSpider(scrapy.Spider, AbstractSpider):
    name = "alternate_nl"

    def __init__(self, **kwargs):
        self.set_webshop_name("AlternateNL")
        AbstractSpider.__init__(self, init_twitter=True)
        super().__init__(**kwargs)

    def start_requests(self):
        urls = [
            {
                "model": GraphicsCardEnum.RTX_3080,
                "link": "https://www.alternate.nl/NVIDIA/GeForce-RTX-3080",
            },
            {
                "model": GraphicsCardEnum.RTX_3090,
                "link": "https://www.alternate.nl/NVIDIA/GeForce-RTX-3080",
            },
        ]
        for url in urls:
            yield scrapy.Request(
                url=url["link"], callback=self.parse, meta={"model": url["model"]}
            )

    def parse(self, response: HtmlResponse, model, **kwargs):
        utc_now = datetime.datetime.utcnow()

        if response.status != 200:
            self.report_error(
                "Response status was not 200 from alternate.",
                utc_now,
                response.url,
                model,
            )
            return

        stock_states = response.css("strong.stockStatus").getall()
        if len(stock_states) == 0:
            self.report_stock_status(
                False, "Not the right html tag.", utc_now, response.url, model
            )
            return

        maybe_stock = False
        for stock_state in stock_states:
            if "Direct leverbaar" in stock_state:
                self.report_stock_status(
                    True,
                    "direct leverbaar is present in the tag.",
                    utc_now,
                    response.url,
                    model,
                )
                return
            if "Preorder" not in stock_state:
                maybe_stock = True

        if maybe_stock:
            self.report_stock_status(
                False,
                "Preorder is not present in the tag.",
                utc_now,
                response.url,
                model,
            )
            return
        self.report_stock_status(
            False, "no stock, only preorder.", utc_now, response.url, model
        )

    def report_stock_status(
        self,
        is_there_stock: bool,
        description: str,
        utc_time: datetime.datetime,
        url: str,
        model,
    ):
        self.report(
            is_there_stock, description, utc_time, url, model, self.webshop_name
        )

    def report_error(self, description: str, utc_time: datetime.datetime, url, model):
        self.report(False, description, utc_time, url, model, self.webshop_name)

    def get_tags_to_add_to_tweet(self, model: GraphicsCardEnum):
        return f"#Alternate #Netherlands {model.value} #Stock #Bot {self.get_common_tags()}"
