import datetime

import scrapy
from scrapy.http import HtmlResponse
from scraper.spiders.abstract_spider import AbstractSpider


class CoolblueSpider(scrapy.Spider, AbstractSpider):
    name = "coolblue_be"

    def __init__(self, **kwargs):
        self.set_webshop_name("CoolblueBE")
        AbstractSpider.__init__(self, init_twitter=True)
        super().__init__(**kwargs)

    def start_requests(self):
        urls = [
            'https://www.coolblue.be/nl/videokaarten/videokaart-chipset:nvidia-geforce-rtx-3080?sorteren=laagste'
            '-prijs&pagina=1 '
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response: HtmlResponse, **kwargs):
        utc_now = datetime.datetime.utcnow()

        if response.status != 200:
            self.report_error("Response status was not 200 from coolblue.", utc_now, response.url)
            return

        stock_states = response.css('div.product-card').css('div.margin-right-2').getall()
        if len(stock_states) == 0:
            self.report_stock_status(False, "Not the right html tag.", utc_now, response.url)
            return

        for stock_state in stock_states:
            if "Morgen in huis" in stock_state:
                self.report_stock_status(True, "Morgen in huis was present in the search result.", datetime.datetime.utcnow(), response.url)

        self.report_stock_status(False, "No available stock", utc_now, response.url)

    def report_stock_status(self, is_there_stock: bool, description: str, utc_time: datetime.datetime, url: str):
        self.report(is_there_stock, description, utc_time, url, self.webshop_name)

    def report_error(self, description: str, utc_time: datetime.datetime, url):
        self.report(False, description, utc_time, url, self.webshop_name)

    def get_tags_to_add_to_tweet(self):
        return f"#Coolblue #Belgium #RTX3080 #Stock #Bot {self.get_common_tags()}"



