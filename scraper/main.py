# this file will run the scrapers
import os
import time

from dotenv import load_dotenv, find_dotenv
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor

from scraper.spiders.alternate_scraper import AlternateSpider
from scraper.spiders.coolblue_scraper import CoolblueSpider
from scraper.spiders.alternate_nl_scraper import AlternateNlSpider

def sleep(_, duration=60 * 30):
    try:
        load_dotenv(find_dotenv())
    except IOError:
        print(".env file was not found.")

    try:
        duration = float(os.getenv("CHECK_EVERY_SECONDS", 60*30))
    except:
        duration = 60*30

    print(f'sleeping for: {duration}')
    time.sleep(duration)  # block here


def crawl(runner):
    print("crawl alternate NL")
    runner.crawl(AlternateNlSpider)
    print("crawl alternate BE")
    runner.crawl(AlternateSpider)
    print("crawl coolblue")
    runner.crawl(CoolblueSpider)
    d = runner.join()
    d.addBoth(sleep)
    d.addBoth(lambda _: crawl(runner))
    return d


def loop_crawl():
    runner = CrawlerRunner(get_project_settings())
    crawl(runner)
    reactor.run()


if __name__ == '__main__':
    print("starting crawl loop.")
    loop_crawl()
