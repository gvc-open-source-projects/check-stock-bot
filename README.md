# Stock checking bot
Will check the stock of certain shops for the RTX 3080 availability.

# Guide
It is made with Scrapy. Pretty basic at the moment.
Just install the requirements (in a virtual environment) and run ```scrapy crawl alternate``` in the commandline.

## Alternate
run ```scrapy crawl alternate``` in the commandline.

## Coolblue
run ```scrapy crawl coolblue``` in the commandline.

# Contributors
- [GeordyVC Linkedin](https://www.linkedin.com/in/geordy-van-cutsem-b35441115/)